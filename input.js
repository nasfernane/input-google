const firstTextToCopy = "Kiff you de ouf";
const secondTextToCopy = "Kiff you more";
const thirdTextToCopy = "Kiffation gravitationnelle à ton encontre";

const firstScreenshot = document.querySelector('#first_screenshot');
const secondScreenshot = document.querySelector('#second_screenshot');
const copyIcon = document.querySelector('.copy-svg')

firstScreenshot.addEventListener('click', function() {
  copy(firstTextToCopy);
}) 

secondScreenshot.addEventListener('click', function() {
  copy(secondTextToCopy);
}) 

copyIcon.addEventListener('click', function() {
  copy(thirdTextToCopy);
}) 

function copy(text) {
  console.log('test', text)
  navigator.clipboard.writeText(text).then(function() {
    console.log('Async: Copying to clipboard was successful!');
  }, function(err) {
    console.error('Async: Could not copy text: ', err);
  });
} 

